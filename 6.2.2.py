from decimal import *


def output(name, n, p, pTrue, D, DTrue):
	print('_' * 50)
	print('Для выборки объема ' + str(n) + ': ')
	print()
	print('Оценка ' + name + ': ' + str(Decimal(p).quantize(Decimal('0.001'))))
	print('Погрешность оценки ' + name + ': ' + str(Decimal(abs(pTrue - p)).quantize(Decimal('0.001'))))
	print('Оценка дисперсии: ' + str(Decimal(D).quantize(Decimal('0.001'))))
	print('Погрешность оценки дисперсии: ' + str(Decimal(abs(DTrue - D)).quantize(Decimal('0.001'))))

def bernoulli():
	p = float(input('Введите параметр p: '))
	output('p', 10, E1, p, D1, p * (1 - p))
	output('p', 10000, E4, p, D4, p * (1 - p))

def binomial():
	n = int(input('Введите параметр n: '))
	p = float(input('Введите параметр p: '))
	output('p', 10, E1 / n, p, D1, n * p * (1 - p))
	output('p', 10000, E4 / n, p, D4, n * p * (1 - p))

def geometric():
	p = float(input('Введите параметр p: '))
	output('p', 10, 1 / E1, p, D1, (1 - p) / p ** 2)
	output('p', 10000, 1 / E4, p, D4, (1 - p) / p ** 2)

def poisson():
	l = float(input('Введите параметр \u03BB: '))
	output('\u03BB', 10, E1, l, D1, l)
	output('\u03BB', 10000, E4, l, D4, l)

def uniform():
	a = float(input('Введите параметр a: '))
	b = float(input('Введите параметр b: '))
	output('a', 10, 2 * E1 - b, a, D1, (b - a) ** 2 / 12)
	output('a', 10000, 2 * E4 - b, a, D4, (b - a) ** 2 / 12)

def exponential():
	l = float(input('Введите параметр \u03BB: '))
	output('\u03BB', 10, 1 / E1, l, D1, 1 / l ** 2)
	output('\u03BB', 10000, 1 / E4, l, D4, 1 / l ** 2)

def normal():
	a = float(input('Введите параметр a: '))
	d = float(input('Введите параметр \u03C3\u00B2: '))
	output('a', 10, E1, a, D1, d)
	output('a', 10000, E4, a, D4, d)


sample = [[], []]
address = [input('Переместите файл с выборкой объема 10 в папку с программой и введите его название без расширения: ')  + '.csv', input('Переместите файл с выборкой объема 10000 в папку с программой и введите его название без расширения: ')  + '.csv']
distribution = address[0][:address[0].find('_')].lower()
for i in range(2):	
	for j in open(address[i]):
		if j[-1] == '\n':
			j = j[:-1]
		sample[i].append(float(j))


E1 = sum(sample[0]) / 10
D1 = sum([(i - E1) ** 2 for i in sample[0]]) / 10
E4 = sum(sample[1]) / 10000
D4 = sum([(i - E4) ** 2 for i in sample[1]]) / 10000
globals()[distribution]()